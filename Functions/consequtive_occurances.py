from itertools import groupby

a = "aaaaaabbbcccccaaaaaaaaaaaaaaaaaadddddooooooooooooooo"

print "max:", max(len(list(y)) for (c,y) in groupby(a)), "\n"

for (c,y) in groupby(a):
    print c, len(list(y))

len_encoding = ''
for c,y in groupby(a):
    len_encoding += str(len(list(y))) + c
print len_encoding