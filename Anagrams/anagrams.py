from collections import Counter

f = open("in.txt","r")

for line in f.readlines():
    removed = 0
    words = map(Counter, line.strip("\n").split(","))
    if words[0] == words[1]:
        print 0
        continue
    for char, count in words[0].items():
        if char not in words[1]:
            removed += words[0][char]
            del words[0][char]
        elif count > words[1][char]:
            removed += count - words[1][char]
            words[0][char] -= count - words[1][char]
    for char, count in words[1].items():
        if char not in words[0]:
            removed += words[1][char]
        elif count > words[0][char]:
            removed += count - words[0][char]
    print removed

